import java.util.Properties;

import org.moeaframework.core.Problem;
import org.moeaframework.core.Variation;
import org.moeaframework.core.operator.OnePointCrossover;
import org.moeaframework.core.operator.TwoPointCrossover;
import org.moeaframework.core.operator.UniformCrossover;
import org.moeaframework.core.operator.binary.BitFlip;
import org.moeaframework.core.operator.binary.HUX;
import org.moeaframework.core.operator.grammar.GrammarCrossover;
import org.moeaframework.core.operator.grammar.GrammarMutation;
import org.moeaframework.core.operator.permutation.Insertion;
import org.moeaframework.core.operator.permutation.PMX;
import org.moeaframework.core.operator.permutation.Swap;
import org.moeaframework.core.operator.program.PointMutation;
import org.moeaframework.core.operator.program.SubtreeCrossover;
import org.moeaframework.core.operator.real.AdaptiveMetropolis;
import org.moeaframework.core.operator.real.DifferentialEvolution;
import org.moeaframework.core.operator.real.PCX;
import org.moeaframework.core.operator.real.PM;
import org.moeaframework.core.operator.real.SBX;
import org.moeaframework.core.operator.real.SPX;
import org.moeaframework.core.operator.real.UM;
import org.moeaframework.core.operator.real.UNDX;
import org.moeaframework.core.operator.subset.Replace;
import org.moeaframework.core.operator.subset.SSX;
import org.moeaframework.core.spi.OperatorProvider;
import org.moeaframework.util.TypedProperties;

public class SDOperatorProvider extends OperatorProvider{

private String mutationHint;
	
	private String variationHint;
	


	@Override
	public String getMutationHint(Problem problem) {
		
		return mutationHint;
	}

	@Override
	public String getVariationHint(Problem problem) {

		return variationHint;
	}

	@Override
	public Variation getVariation(String name, Properties properties, Problem problem) {
		
		TypedProperties typedProperties = new TypedProperties(properties);
		
		if (name.equalsIgnoreCase("sbx")) {
			return new SBX(
					typedProperties.getDouble("sbx.rate", 1.0), 
					typedProperties.getDouble("sbx.distributionIndex", 15.0),
					typedProperties.getBoolean("sbx.swap", true),
					typedProperties.getBoolean("sbx.symmetric", false));
		} else if (name.equalsIgnoreCase("pm")) {
			return new PM(
					typedProperties.getDouble("pm.rate", 
							1.0 / problem.getNumberOfVariables()), 
							typedProperties.getDouble("pm.distributionIndex", 20.0));
		} else if (name.equalsIgnoreCase("de")) {	
			return new DifferentialEvolution(
					typedProperties.getDouble("de.crossoverRate", 0.1), 
					typedProperties.getDouble("de.stepSize", 0.5));
		} else if (name.equalsIgnoreCase("pcx")) {
			return new PCX(
					(int)typedProperties.getDouble("pcx.parents", 2),
					(int)typedProperties.getDouble("pcx.offspring", 2), 
					typedProperties.getDouble("pcx.eta", 0.1), 
					typedProperties.getDouble("pcx.zeta", 0.1));
		} else if (name.equalsIgnoreCase("spx")) {
			return new SPX(
					(int)typedProperties.getDouble("spx.parents", 2),
					(int)typedProperties.getDouble("spx.offspring", 2),
					typedProperties.getDouble("spx.epsilon", 3));
		} else if (name.equalsIgnoreCase("undx")) {
			return new UNDX(
					(int)typedProperties.getDouble("undx.parents", 2),
					(int)typedProperties.getDouble("undx.offspring", 2), 
					typedProperties.getDouble("undx.zeta", 0.5), 
					typedProperties.getDouble("undx.eta", 0.35));
		} else if (name.equalsIgnoreCase("um")) {
			return new UM(
					typedProperties.getDouble("um.rate", 
							1.0 / problem.getNumberOfVariables()));
		} else if (name.equalsIgnoreCase("am")) {
			return new AdaptiveMetropolis(
					(int)typedProperties.getDouble("am.parents", 2),
					(int)typedProperties.getDouble("am.offspring", 2), 
					typedProperties.getDouble("am.coefficient", 2.4));
		} else if (name.equalsIgnoreCase("hux")) {
			return new HUX(
					typedProperties.getDouble("hux.rate", 1.0));
		} else if (name.equalsIgnoreCase("bf")) {
			return new BitFlip(
					typedProperties.getDouble("bf.rate", 0.01));
		} else if (name.equalsIgnoreCase("pmx")) {
			return new PMX(
					typedProperties.getDouble("pmx.rate", 1.0));
		} else if (name.equalsIgnoreCase("insertion")) {
			return new Insertion(
					typedProperties.getDouble("insertion.rate", 0.3));
		} else if (name.equalsIgnoreCase("swap")) {
			return new Swap(
					typedProperties.getDouble("swap.rate", 0.3));
		} else if (name.equalsIgnoreCase("1x")) {
			return new OnePointCrossover(
					typedProperties.getDouble("1x.rate", 1.0));
		} else if (name.equalsIgnoreCase("2x")) {
			return new TwoPointCrossover(
					typedProperties.getDouble("2x.rate", 1.0));
		} else if (name.equalsIgnoreCase("ux")) {
			return new UniformCrossover(
					typedProperties.getDouble("ux.rate", 1.0));
		} else if (name.equalsIgnoreCase("gx")) {
			return new GrammarCrossover(
					typedProperties.getDouble("gx.rate", 1.0));
		} else if (name.equalsIgnoreCase("gm")) {
			return new GrammarMutation(
					typedProperties.getDouble("gm.rate", 1.0));
		} else if (name.equalsIgnoreCase("ptm")) {
			return new PointMutation(
					typedProperties.getDouble("ptm.rate", 0.01));
		} else if (name.equalsIgnoreCase("bx")) {
			return new SubtreeCrossover(
					typedProperties.getDouble("bx.rate", 0.9));
		} else if (name.equalsIgnoreCase("replace")) {
			return new Replace(
					typedProperties.getDouble("replace.rate", 0.9));
		} else if (name.equalsIgnoreCase("ssx")) {
			return new SSX(
					typedProperties.getDouble("ssx.rate", 0.3));
		} else {
			return null;
		}
	}

	

	public String getMutationHint() {
		return mutationHint;
	}

	public void setMutationHint(String mutationHint) {
		this.mutationHint = mutationHint;
	}

	public String getVariationHint() {
		return variationHint;
	}

	public void setVariationHint(String variationHint) {
		this.variationHint = variationHint;
	}
	

}
