# -*- coding: utf-8 -*-
"""
Created on Thu Jul 16 17:55:03 2020

@author: Igor
"""

import pandas as pd
import os
from sklearn.preprocessing import MinMaxScaler
import numpy as np



base_path = os.getcwd()
data_folder = 'selected data'

welch_features = {'Delta': 0, 'Theta': 1, 'Alpha': 2, 'Beta': 3, 'Sigma': 4, 'Gamma': 5, 'Alpha1': 6, 'Alpha2': 7}
haar_features = {'Delta': 0, 'Theta': 1, 'Alpha': 2, 'Beta': 3, 'Gamma': 4}
channels = {'F3': 0, 'F4': 1, 'C3': 2, 'C4': 3, 'O1': 4, 'O2': 5}


def limit_max_change(data, max_change=1.1):
    """
    max change between two neighbours can be max_chenge times (default x2)
    """
    result = []
    for i, val in enumerate(data):
        if i==0:
            result.append(val)
            continue
        elif val > max_change*result[i-1]:
            result.append(max_change*result[i-1])
        elif val < (1/max_change)*result[i-1]:
            result.append((1/max_change)*result[i-1])
        else:
            result.append(val)
    return result


def limit_change_std(data):
    """
    max change between two neighbours depends of std of data
    """
    std = np.std(data)
    result = []
    for i, val in enumerate(data):
        if i==0:
            result.append(val)
            continue
        elif val > std+result[i-1]:
            result.append(std+result[i-1])
        elif val < result[i-1]-std:
            result.append(result[i-1]-std)
        else:
            result.append(val)
    return result


def scaler(data):
    """
    Scales data into interval from 0 to 1.
    """
    scaler = MinMaxScaler()
    return scaler.fit_transform(data)


def get_welch_freq():
    """
    Get PSD features obtained with welch method.
    
    Returns list with dimensions 16x6x238x8
    16 subjects, 6 channels, 238 epochs, 8 features (see header variable)
    """
    path = '\\'.join((base_path, data_folder))
    header = ['Delta', 'Theta', 'Alpha', 'Beta', 'Sigma', 'Gamma', 'Alpha1', 'Alpha2']
    result = []
    
    directories = [x[1] for x in os.walk(path)]
    directories = directories[0]
    for d in directories: # all dirs
        current_path = '\\'.join((path, d))
        channels = []
        for f in os.listdir(current_path):
            if '_welch' not in f:
                continue
            file_name = '\\'.join((current_path, f))
            df = pd.read_csv(file_name, names=header)
            
            channels.append(df.values)
        result.append(channels)
        
    return result


def get_haar_freq():
    """
    Get PSD features obtained with Haar wavelet method.
    
    Returns list with dimensions 16x6x238x8
    16 subjects, 6 channels, 238 epochs, 5 features (see header variable)
    """
    path = '\\'.join((base_path, data_folder))
    header = ['Delta', 'Theta', 'Alpha', 'Beta', 'Gamma']
    result = []
    
    directories = [x[1] for x in os.walk(path)]
    directories = directories[0]
    for d in directories: # all dirs
        current_path = '\\'.join((path, d))
        channels = []
        for f in os.listdir(current_path):
            if '_Haar' not in f:
                continue
            file_name = '\\'.join((current_path, f))
            df = pd.read_csv(file_name, names=header)
            
            channels.append(df.values)
        result.append(channels)
        
    return result

    
def get_novel_index_1(scale=False, max_change=None):
    """
    Returns novel index for all subjects
    """
    welch = get_welch_freq()
    results = []
    
    for subject in welch:
        brojnik = (1 * subject[channels['F3']][:, welch_features['Alpha']] + 
                   4 * subject[channels['O2']][:, welch_features['Alpha']] + 
                   9 * subject[channels['F3']][:, welch_features['Alpha1']] + 
                   3 * subject[channels['C3']][:, welch_features['Alpha1']] + 
                   9 * subject[channels['C4']][:, welch_features['Alpha1']] + 
                   1 * subject[channels['O2']][:, welch_features['Alpha1']] + 
                   4 * subject[channels['O1']][:, welch_features['Alpha2']] + 
                   8 * subject[channels['O2']][:, welch_features['Alpha2']])
        nazivnik = (1 * subject[channels['F3']][:, welch_features['Delta']] +
                    3 * subject[channels['F4']][:, welch_features['Delta']] +
                    3 * subject[channels['C3']][:, welch_features['Delta']] +
                    2 * subject[channels['C4']][:, welch_features['Delta']] +
                    9 * subject[channels['O2']][:, welch_features['Delta']])
        tmp = brojnik/nazivnik
        if max_change:
            tmp = limit_max_change(tmp, max_change=max_change)
        if scale:
            tmp = scaler(np.array(tmp).reshape(-1, 1)).reshape(1,-1)[0]
            tmp = tmp.tolist()
        results.append(tmp)
    return results


def get_novel_index_2(scale=False, max_change=None):
    """
    Returns novel index for all subjects
    """
    welch = get_welch_freq()
    results = []
    
    for subject in welch:
        brojnik = (subject[channels['F3']][:, welch_features['Delta']] + 
                   subject[channels['F4']][:, welch_features['Delta']] + 
                   subject[channels['O2']][:, welch_features['Delta']])
        nazivnik = (subject[channels['C3']][:, welch_features['Alpha']] +
                    subject[channels['O2']][:, welch_features['Alpha2']])
        tmp = brojnik/nazivnik
        if max_change:
            tmp = limit_max_change(tmp, max_change=max_change)
        if scale:
            tmp = scaler(np.array(tmp).reshape(-1, 1)).reshape(1,-1)[0]
            tmp = tmp.tolist()
        results.append(tmp)
    return results

def get_t_a(scale=False, max_change=None):
    """
    theta/alpha
    """
    welch = get_welch_freq()
    results = []
    
    for subject in welch:
        mid_res = []
        for ch, value in channels.items():
            brojnik = subject[value][:,welch_features['Theta']]
            nazivnik = subject[value][:,welch_features['Alpha']]
            tmp = brojnik/nazivnik
            if max_change:
                tmp = limit_max_change(tmp, max_change=max_change)
            if scale:
                tmp = scaler(np.array(tmp).reshape(-1, 1)).reshape(1,-1)[0]
                tmp = tmp.tolist()
            mid_res.append(tmp)
        results.append(mid_res)
    return results

def get_b_a(scale=False, max_change=None):
    """
    beta/alpha
    """
    welch = get_welch_freq()
    results = []
    
    for subject in welch:
        mid_res = []
        for ch, value in channels.items():
            brojnik = subject[value][:,welch_features['Beta']]
            nazivnik = subject[value][:,welch_features['Alpha']]
            tmp = brojnik/nazivnik
            if max_change:
                tmp = limit_max_change(tmp, max_change=max_change)
            if scale:
                tmp = scaler(np.array(tmp).reshape(-1, 1)).reshape(1,-1)[0]
                tmp = tmp.tolist()
            mid_res.append(tmp)
        results.append(mid_res)
    return results

def get_ta_b(scale=False, max_change=None):
    """
    theta+alpha/beta
    """
    welch = get_welch_freq()
    results = []
    
    for subject in welch:
        mid_res = []
        for ch, value in channels.items():
            brojnik = (subject[value][:,welch_features['Theta']] +
                       subject[value][:,welch_features['Alpha']])
            nazivnik = subject[value][:,welch_features['Beta']]
            tmp = brojnik/nazivnik
            if max_change:
                tmp = limit_max_change(tmp, max_change=max_change)
            if scale:
                tmp = scaler(np.array(tmp).reshape(-1, 1)).reshape(1,-1)[0]
                tmp = tmp.tolist()
            mid_res.append(tmp)
        results.append(mid_res)
    return results

def get_t_b(scale=False, max_change=None):
    """
    theta/beta
    """
    welch = get_welch_freq()
    results = []
    
    for subject in welch:
        mid_res = []
        for ch, value in channels.items():
            brojnik = subject[value][:,welch_features['Theta']]
            nazivnik = subject[value][:,welch_features['Beta']]
            tmp = brojnik/nazivnik
            if max_change:
                tmp = limit_max_change(tmp, max_change=max_change)
            if scale:
                tmp = scaler(np.array(tmp).reshape(-1, 1)).reshape(1,-1)[0]
                tmp = tmp.tolist()
            mid_res.append(tmp)
        results.append(mid_res)
    return results

def get_ta_ab(scale=False, max_change=None):
    """
    theta+alpha/alpha+beta
    """
    welch = get_welch_freq()
    results = []
    
    for subject in welch:
        mid_res = []
        for ch, value in channels.items():
            brojnik = (subject[value][:,welch_features['Theta']] +
                       subject[value][:,welch_features['Alpha']])
            nazivnik = (subject[value][:,welch_features['Beta']] +
                       subject[value][:,welch_features['Alpha']])
            tmp = brojnik/nazivnik
            if max_change:
                tmp = limit_max_change(tmp, max_change=max_change)
            if scale:
                tmp = scaler(np.array(tmp).reshape(-1, 1)).reshape(1,-1)[0]
                tmp = tmp.tolist()
            mid_res.append(tmp)
        results.append(mid_res)
    return results

def get_g_d(scale=False, max_change=None):
    """
    gamma/delta
    """
    haar = get_haar_freq()
    results = []
    
    for subject in haar:
        mid_res = []
        for ch, value in channels.items():
            brojnik = subject[value][:,haar_features['Gamma']]
            nazivnik = subject[value][:,haar_features['Delta']]
            tmp = brojnik/nazivnik
            if max_change:
                tmp = limit_max_change(tmp, max_change=max_change)
            if scale:
                tmp = scaler(np.array(tmp).reshape(-1, 1)).reshape(1,-1)[0]
                tmp = tmp.tolist()
            mid_res.append(tmp)
        results.append(mid_res)
    return results

def get_gb_da(scale=False, max_change=None):
    """
    gamma+beta/delta+alpha
    """
    haar = get_haar_freq()
    results = []
    
    for subject in haar:
        mid_res = []
        for ch, value in channels.items():
            brojnik = (subject[value][:,haar_features['Gamma']] +
                       subject[value][:,haar_features['Beta']])
            nazivnik = (subject[value][:,haar_features['Delta']] +
                       subject[value][:,haar_features['Alpha']])
            tmp = brojnik/nazivnik
            if max_change:
                tmp = limit_max_change(tmp, max_change=max_change)
            if scale:
                tmp = scaler(np.array(tmp).reshape(-1, 1)).reshape(1,-1)[0]
                tmp = tmp.tolist()
            mid_res.append(tmp)
        results.append(mid_res)
    return results

def get_all_features():
    results = []
    results.append(get_novel_index_1(scale=True, max_change=1.1))
    results.append(get_novel_index_2(scale=True, max_change=1.1))
    results.append(get_t_a())
    results.append(get_b_a())
    results.append(get_ta_b())
    results.append(get_t_b())
    results.append(get_ta_ab())
    results.append(get_g_d())
    results.append(get_gb_da())
    
    for r in results:
        yield r
  
if __name__ == '__main__':
    #haar = get_haar_freq()
    #welch = get_welch_freq()
    ff = get_gb_da()
    