import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.moeaframework.Executor;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Solution;
import org.moeaframework.core.spi.OperatorFactory;
import org.moeaframework.core.variable.EncodingUtils;

public class main {

	public static void main(String[] args) {
		
		
		if (args.length<3 || args[0].contains("help") || args[0].contains("-h")){
			System.out.println("\r\n***********************************************************\r\n");
			System.out.println("SDProblem v.0.1");
			System.out.println("\r\nUsage: java -jar SDProblem.jar [params_file] [res_file] [eval_nr]");
			System.out.println("--Example: java -jar SDProblem.jar ./params.txt ./res.txt 100000");
			System.out.println("\r\n***********************************************************\r\n");
			
			return;
		}
		
		String paramsFile = args[0]; 
		String outputResFile = args[1];
		int maxEvals = Integer.parseInt(args[2]); 
		
		List<String> lines;
		try {
			lines = Files.readAllLines(Paths.get(paramsFile));
			String dataDir = lines.get(0);
			String signalsBrojnik = lines.get(1);
			String signalsNazivnik = lines.get(2);
			String varHint = lines.get(3);
			String mutHint = lines.get(4);
			String coeffbound = lines.get(5);
			
			List<Trace> traces = InputParser.parseTraces(dataDir);
			
			Equation eq= new Equation();
			
			eq.setBrojnik(InputParser.parseEquationVars(signalsBrojnik));
			eq.setNazivnik(InputParser.parseEquationVars(signalsNazivnik));

						
			SDProblem p = new SDProblem(traces,eq, Double.parseDouble(coeffbound)); 
			SDOperatorProvider prov = new SDOperatorProvider();
			OperatorFactory.getInstance().addProvider(prov);
			prov.setVariationHint(varHint);
			prov.setMutationHint(mutHint);
			
			NondominatedPopulation res = new Executor().withAlgorithm("NSGA-II").withProblem(p).withMaxEvaluations(maxEvals).run();
	

			int k=0;
			for(Solution solutio : res){
				
				try
				{
									
					//write down solutions 
				    FileWriter fw;
				    if(solutio.getObjective(1)== Double.POSITIVE_INFINITY)
				    	fw= new FileWriter(outputResFile+"_"+k+"_ifty.txt",false); //the true will append the new data
				    else
				    	fw= new FileWriter(outputResFile+"_"+k+".txt",false); 
				    fw.write("O1: "+ solutio.getObjective(0) +"; " + "O2: "+ solutio.getObjective(1));//appends the string to the file
				    fw.write("\r\n#########\r\n");
				    
				   
				    //set coeffs 
					int n=0;
					for(int m=0; m< eq.getBrojnik().length;m++) {
						if(eq.getBrojnik()[m]==0)
							eq.getBrojnikCoeffs()[m]=0;
						else {
							eq.getBrojnikCoeffs()[m]=EncodingUtils.getReal(solutio.getVariable(n));
							n++;
						}
					}
					
					for(int m=0; m< eq.getNazivnik().length;m++) {
						if(eq.getNazivnik()[m]==0)
							eq.getNazivnikCoeffs()[m]=0;
						else {
							eq.getNazivnikCoeffs()[m]=EncodingUtils.getReal(solutio.getVariable(n));
							n++;
						}
					}
					
					
					//write coeffs 
					fw.write("# Brojnik:\r\n");
				    for(int i=0;i<eq.getBrojnikCoeffs().length;i++) {
				    	Double coeff = eq.getBrojnikCoeffs()[i];
				    	fw.write(coeff.toString());
				    	fw.write("\r\n");
				    }
				    
					fw.write("# Nazivnik:\r\n");
				    for(int i=0;i<eq.getNazivnikCoeffs().length;i++) {
				    	Double coeff = eq.getNazivnikCoeffs()[i];
				    	fw.write(coeff.toString());
				    	fw.write("\r\n");
				    }
					
					
				    
					for(int i=0; i<traces.size(); i++) {	
						
						fw.write("# Vrijednosti funkcije i vrijednosti trenda funkcije za trace " + i + ":\r\n");
					
						//calculate trace vals for each trace and print out
						//List<Double> calculation = eq.calculateTrace(traces.get(i));
						
						List<Double> calculation = eq.calculateTrace2(traces.get(i));
						
						double [] trend = p.getTrend(traces.get(i));		
						
						
						fw.write("?\r\n");
						for(int j=0; j<trend.length; j++) {
							Double tempRes = trend[j];
							fw.write(calculation.get(j).toString() + ", " + tempRes.toString());
							fw.write("\r\n");
						}
						fw.write("?\r\n");
					
					}
					
						
				    k++;
				    fw.close();
				    
				    
				}
				catch(IOException ioe)
				{
				    System.err.println("IOException: " + ioe.getMessage());
				    ioe.printStackTrace();
				}				
								

		
			}

			
	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
