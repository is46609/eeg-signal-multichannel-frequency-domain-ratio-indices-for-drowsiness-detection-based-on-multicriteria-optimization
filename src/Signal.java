import java.util.ArrayList;
import java.util.List;

public class Signal {

	private SignalType type;
	private List<Channel> channels;
	
	
	public Signal(SignalType type) {
		this.type=type;
		this.channels = new ArrayList<Channel>();
		channels.add(new Channel(0));
		channels.add(new Channel(1));
		channels.add(new Channel(2));
		channels.add(new Channel(3));
		channels.add(new Channel(4));
		channels.add(new Channel(5));
	}
	
	public SignalType getSignalType() {
		return type;
	}
	
	public  List<Channel> getChannels(){
		return channels;
	}
	

}
