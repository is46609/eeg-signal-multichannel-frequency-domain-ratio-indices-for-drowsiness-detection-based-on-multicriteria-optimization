# -*- coding: utf-8 -*-
"""
Created on Thu Mar 21 09:17:43 2019

@author: Giggs
"""

import os
import numpy as np
import pandas as pd
from scipy import signal
from scipy.io import loadmat
import matplotlib.pyplot as plt
from spectrum import pmtm
import pywt


class Signal():
    
    def __init__(self, record_number="tr03-0052", record_name='raw_filtered_1_40', base_path='D:\\FER\\Doktorski\\Doktorski\\Spt Spc Entropy\\src\\selected data\\',
                 sufix='.csv', separator='\\', fs=200, ch_num=6,
                 signal_len=None, epoch_sec=10, epoch_overlap=0.5, 
                 method='welch',
                 eeg_bands = {'Delta': (1, 4), 'Theta': (4, 8), 'Alpha': (8, 12), 
                 'Beta': (12, 30), 'Sigma':(12, 14), 'Gamma':(30, 49), 
                 'Alpha1':(8, 10), 'Alpha2': (10, 12)}):
        '''
        Constructor that intialize all needed constants, imports data,
        splits data into epochs and calculate power for each band.
        
        @param ...
        '''
        # Path setup
        self.base_path = base_path
        self.record_number = record_number
        self.record_name = record_name
        self.method = method
        self.sufix = sufix
        self.separator = separator
        self.non_overlaping_bands = {'Delta': (1, 4), 'Theta': (4, 8),
                                     'Alpha1':(8, 10), 'Alpha2': (10, 12), 
                                     'Sigma':(12, 14), 'Beta':(14,30), 'Gamma':(30, 49)}
        
        # Import signal # changed 1.11. - now importing from csv
#==============================================================================
#         self.signal = loadmat(self.base_path + self.record_number + self.separator + 
#                               self.record_number + self.sufix)
#         self.signal = self.signal['sig'][:,0:-1] # Hardcoded for my data
#==============================================================================
        # Import signal from csv
        df = pd.read_csv(self.base_path + self.record_number + self.separator + 
                                self.record_name + self.sufix, header=None)
        self.signal = df.iloc[:,:].values
        del df

        #
        self.fs = fs
        if ch_num != len(self.signal): # assert
            raise ValueError
        else:
            self.ch_num = ch_num
        self.epoch_sec = epoch_sec
        if signal_len != None: 
            self.signal_len = signal_len
        else:
            self.signal_len = len(self.signal[0])
        self.epoch_len = self.fs * self.epoch_sec
        self.epoch_overlap = epoch_overlap
        self.eeg_bands = eeg_bands
        self.create_epochs()
        if self.method != 'Haar':
            self.calculate_all_psd(method=self.method)
        else:
            self.calculate_all_psd_wavelet(method=method)
        
        
    def create_epochs(self):
        '''
        Split signal into epochs
        '''
        # Create list with starts of each epoch
        step = int(self.epoch_len * self.epoch_overlap)
        if step == 0:
            step = int(self.epoch_len)
        self.epoch_start = (list(range(0, self.signal_len, step)))
        
        # Drop x last elements from list that don't satisfy condition
        for ix, start in enumerate(np.flip(self.epoch_start, 0)):
            if self.signal_len - start < self.epoch_len:
                continue
            else:
                self.epoch_start = self.epoch_start[0:(-(ix+1))]
                break
            
        # Split to epochs
        self.epoch_num = len(self.epoch_start)
        self.epoch = np.zeros((self.ch_num, self.epoch_num, self.epoch_len))
        for en, es in enumerate(self.epoch_start):
            for ch in range(self.ch_num):
                self.epoch[ch][en] = self.signal[ch, es:es+self.epoch_len]
                
                
    def create_epochs_wavelet(self):
        '''
        Split signal into epochs for wavelet
        Difference is that this time epochs are using 12 bids from
        previous epoch
        '''
        # Create list with starts of each epoch
        step = int(self.epoch_len * self.epoch_overlap)
        if step == 0:
            step = int(self.epoch_len)
        self.epoch_start = (list(range(0, self.signal_len, step)))
        
        # Drop x last elements from list that don't satisfy condition
        for ix, start in enumerate(np.flip(self.epoch_start, 0)):
            if self.signal_len - start < self.epoch_len:
                continue
            else:
                self.epoch_start = self.epoch_start[0:(-(ix+1))]
                break
        
        # Drop first because there is no 12 bids before it
        self.epoch_start = self.epoch_start [1:]
            
        # additional bids
        add_bids = 12
        
        # Split to epochs
        self.epoch_num = len(self.epoch_start)
        self.epoch_wavelet = np.zeros((self.ch_num, self.epoch_num, self.epoch_len + add_bids))
        for en, es in enumerate(self.epoch_start):
            for ch in range(self.ch_num):
                self.epoch_wavelet[ch][en] = self.signal[ch, es-add_bids : es+self.epoch_len]
                
                
    def resampling(self, new_fs):
        new_data = np.empty((self.ch_num, int(len(self.signal[0])/2)))
        for ch in range(self.ch_num):
            data = self.signal[ch,:]
            x = np.arange(0, len(data))
            new_x = np.arange(0, len(data), self.fs/new_fs)
            new_data[ch,:] = np.interp(new_x, x, data)
            
        # store new data
        self.signal = new_data
        self.signal_len = len(self.signal[0])
        self.fs = new_fs
        self.epoch_len = self.fs * self.epoch_sec


    def calculate_psd_multitapering(self, data, fs=None, eeg_bands=None, 
                                    plot=False, NW=4, k=6):
        '''
        Function calculates PSD with multitapering.
        
        https://raphaelvallat.com/bandpower.html
        https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5343535/
        
        http://thomas-cokelaer.info/software/spectrum/html/user/ref_mtm.html
        https://stackoverflow.com/questions/50655953/understanding-the-frequency-axis-of-multitaper-method-in-python
        
        @param data raw data
        @param fs sampling freq
        @param eeg_bands freq bands that will be calculated
        @return eeg_band_psd dict with psd per band
        '''
        # Get
        if fs == None:
            fs = self.fs
        if eeg_bands == None:
            eeg_bands = self.eeg_bands
            
        # Multitapering
        Sk_complex, weights, eigenvalues = pmtm(data, NW=NW, k=k, show=plot)
        eeg_band_psd = np.mean((abs(Sk_complex)**2).transpose()*weights, axis=1)
        
        return eeg_band_psd


    def calculate_psd_Haar(self, data, fs=None, eeg_bands=None,
                              plot=False):
        '''
        Calculate wavelet bands and PSD (from full decomposition)
        based on da Silveira paper in Expert System with Applications.
        '''
        def pfwc(coefs):
            # Full name - power_from_wavelet_coefs
            res = np.abs(coefs)
            res = np.square(res)
            res = np.sum(res)
            return res
        
        def gd(packet, level, row, details):
            # get details from wanted wavelet decomposition
            return packet.get_level(level)[row].decompose()[details].data
        
        # Get
        if fs == None:
            fs = self.fs
        if eeg_bands == None:
            eeg_bands = self.eeg_bands
        
        result = dict()
        # Haar wavelet packet (full tree) - Hardcodirano da bude isto ko u da Silveira 2016
        p = pywt.WaveletPacket(data, 'Haar')
        import pdb
        pdb.set_trace()
        #############
        # Ovo je loša implementacija jer se pozicionira na level iznad željenog
        # i onda radi dekompoziciju tog levela i gleda rješenje.
        # Wavelet Packet odmah napravi dekompoziciju svega i svi
        # walici su vec izracunati te se možemo samo kretat po wavelet packet
        # stablu i računati površine.
        #############
        result['Delta'] = pfwc(gd(p,4,0,1)) + pfwc(gd(p,5,0,1)) + pfwc(gd(p,7,1,1)) + pfwc(gd(p,5,2,0))
        result['Theta'] = 0
        result['Alpha'] = pfwc(gd(p,3,1,1)) + pfwc(gd(p,5,5,1)) + pfwc(gd(p,6,10,1)) + pfwc(gd(p,6,16,0)) + pfwc(gd(p,7,20,1)) + pfwc(gd(p,8,66,0))
        result['Beta'] = pfwc(gd(p,2,1,1)) + pfwc(gd(p,3,2,1)) + pfwc(gd(p,3,4,0)) + pfwc(gd(p,4,4,1)) + pfwc(gd(p,4,9,0)) + pfwc(gd(p,5,8,1)) + pfwc(gd(p,7,126,0)) + pfwc(gd(p,7,33,1)) + pfwc(gd(p,8,153,0))
        #result['Sigma'] = 
        result['Gamma'] = pfwc(gd(p,1,1,1)) + pfwc(gd(p,2,2,1)) + pfwc(gd(p,5,19,1)) + pfwc(gd(p,6,38,1))
        #result['Alpha1'] = 
        #result['Alpha2'] = 
        
        return result
        


    def calculate_psd_welch(self, data, fs=None, eeg_bands=None,
                                  plot=False):
        '''
        Function calculates PSD with periodogram.
        
        @param data raw data
        @param fs sampling freq
        @param eeg_bands freq bands that will be calculated
        @return eeg_band_psd dict with psd per band
        '''
        # Get
        if fs == None:
            fs = self.fs
        if eeg_bands == None:
            eeg_bands = self.eeg_bands
            
        # Define window length (4 sec)
        #win = 4 * fs
        win = 1.5*fs # dodano 1.12.
#==============================================================================
#         win = int((1/3) * len(data)) # Promjenio 14.10. Prije bilo ovo iznad
#==============================================================================
        
        tmp_freq, vals = signal.welch(data, fs, nperseg=win)
        
        # Interpoliraj da dobiš više točaka za preciznije računanje
        # površine ispod krivulje
        freq = np.array(list(range(int(tmp_freq[0]), int(tmp_freq[-1]*10))))/10
        vals = np.interp(freq, tmp_freq, vals)
        
        
        
        
#==============================================================================
#         freq, vals = signal.welch(data, fs, nperseg=len(data))
#         freq1, vals1 = signal.welch(data, fs, nperseg=1.5*fs)
#         freq2, vals2 = signal.welch(data, fs, nperseg=win)
#         plt.plot(freq[:150],vals[:150])
#         plt.plot(freq1[:100],vals1[:100])
#         plt.plot(freq2[:50],vals2[:50])
#         plt.show()
#         import pdb
#         pdb.set_trace()
#==============================================================================
        
#==============================================================================
#         import pdb
#         pdb.set_trace()
#         mt = pmtm(data, NW=2,show=False)
#         xx = []
#         for i in range(len(mt[1][0])):
#             xx.append(fs/(2*i))
#         plt.plot(xx,mt[1][0])
#         plt.show()
#         import pdb
#         pdb.set_trace()
#==============================================================================
        
        # Take the mean of the fft amplitude for each EEG band
        eeg_band_psd = self.__mean_per_band(freq, vals, eeg_bands)
        
        
        # Plot and return
        if plot:
            plt.plot(freq, vals)
            plt.show()
            
        return eeg_band_psd
        
        
    def calculate_psd_periodogram(self, data, fs=None, eeg_bands=None,
                                  plot=False):
        '''
        Function calculates PSD with periodogram.
        
        @param data raw data
        @param fs sampling freq
        @param eeg_bands freq bands that will be calculated
        @return eeg_band_psd dict with psd per band
        '''
        # Get
        if fs == None:
            fs = self.fs
        if eeg_bands == None:
            eeg_bands = self.eeg_bands
            
        # Calculate periodogram
        freq, vals = signal.periodogram(data, fs)
        
        # Take the mean of the fft amplitude for each EEG band
        eeg_band_psd = self.__mean_per_band(freq, vals, eeg_bands)
        
        # Plot and return
        if plot:
            plt.plot(freq, vals)
            plt.show()
            
        return eeg_band_psd


    def calculate_psd_fft(self, data, fs=None, eeg_bands=None, plot=False):
        '''
        Function calculates PSD for each freq band in a single epoch
        with usage of fft. 
        I'm not sure if this is good function. It is from:
        (https://dsp.stackexchange.com/questions/45345/how-to-correctly-
        compute-the-eeg-frequency-bands-with-python)
                
        @param data raw data
        @param fs sampling frequency
        @param eeg_bands freq bands that will be calculated
        @return eeg_band_fft dict with psd per bands
        '''
        # Get
        if fs == None:
            fs = self.fs
        if eeg_bands == None:
            eeg_bands = self.eeg_bands
            
        # Get real amplitudes of FFT (only in postive frequencies)
        vals = np.abs(np.fft.rfft(data))
        
        # Get frequencies for amplitudes in Hz
        freq = np.fft.rfftfreq(len(data), 1.0/fs)
        
        # Take the mean of the fft amplitude for each EEG band
        eeg_band_fft = self.__mean_per_band(freq, vals, eeg_bands)
        
        # Plot and return
        if plot:
            plt.plot(freq, vals)
            plt.show()
            
        return eeg_band_fft
        
        
    def __mean_per_band(self, freq, vals, bands):
        '''
        Calculate mean PSD per each band.
        
        @param freq frequencies
        @param vals value for each freq
        @param bands dict with name of bands and its freq range
        @return eeg_band_psd power per each band
        '''
        sum_of_all_bands = 0 # For normalization
        eeg_band_psd = dict()
        result = dict()
        for band in self.non_overlaping_bands:
            freq_ix = np.where((freq >= self.non_overlaping_bands[band][0]) & 
                               (freq < self.non_overlaping_bands[band][1]))[0]
            eeg_band_psd[band] = np.mean(vals[freq_ix])
            sum_of_all_bands += eeg_band_psd[band]
        
        # Normalization
        for band in self.non_overlaping_bands:
            eeg_band_psd[band] = eeg_band_psd[band] / sum_of_all_bands

        result['Delta'] = eeg_band_psd['Delta']
        result['Theta'] = eeg_band_psd['Theta']
        result['Alpha'] = eeg_band_psd['Alpha1'] + eeg_band_psd['Alpha2']
        result['Beta'] = eeg_band_psd['Sigma'] + eeg_band_psd['Beta']
        result['Sigma'] = eeg_band_psd['Sigma']
        result['Gamma'] = eeg_band_psd['Gamma']
        result['Alpha1'] = eeg_band_psd['Alpha1']
        result['Alpha2'] = eeg_band_psd['Alpha2']
        
        return result
        
        
    def calculate_all_psd(self, method='welch'):
        '''
        Function that calculates PSD for each epoch in self.epoch
        '''
        # Which method to use
        method_name = 'calculate_psd_' + method
        mtd = getattr(self, method_name)
        
        # Calculate all
        self.band = np.zeros((self.ch_num, self.epoch_num, len(self.eeg_bands)))
        for ch in range(self.ch_num):
            for e in range(self.epoch_num):
                self.band[ch, e, 0] = mtd(self.epoch[ch, e, :], self.fs, self.eeg_bands)['Delta']
                self.band[ch, e, 1] = mtd(self.epoch[ch, e, :], self.fs, self.eeg_bands)['Theta']
                self.band[ch, e, 2] = mtd(self.epoch[ch, e, :], self.fs, self.eeg_bands)['Alpha']
                self.band[ch, e, 3] = mtd(self.epoch[ch, e, :], self.fs, self.eeg_bands)['Beta']
                self.band[ch, e, 4] = mtd(self.epoch[ch, e, :], self.fs, self.eeg_bands)['Sigma']
                self.band[ch, e, 5] = mtd(self.epoch[ch, e, :], self.fs, self.eeg_bands)['Gamma']
                self.band[ch, e, 6] = mtd(self.epoch[ch, e, :], self.fs, self.eeg_bands)['Alpha1']
                self.band[ch, e, 7] = mtd(self.epoch[ch, e, :], self.fs, self.eeg_bands)['Alpha2']


    def calculate_all_psd_wavelet(self, method='Haar', resampling_fs=100):
        # Resampling
        self.resampling(resampling_fs)
        
        # Create wavelet epochs
        self.create_epochs_wavelet()
        
        # Which method to use
        method_name = 'calculate_psd_' + method
        mtd = getattr(self, method_name)
        
        # Calculate all
        self.band = np.zeros((self.ch_num, self.epoch_num, len(self.eeg_bands)))
        for ch in range(self.ch_num):
            for e in range(self.epoch_num):
                tmp = mtd(self.epoch_wavelet[ch, e, :], self.fs, self.eeg_bands)
                self.band[ch, e, 0] = tmp['Delta']
                self.band[ch, e, 2] = tmp['Alpha']
                self.band[ch, e, 3] = tmp['Beta']
                self.band[ch, e, 4] = tmp['Gamma']
                


if __name__ == '__main__':
#==============================================================================
#     record = Signal(epoch_sec=1, epoch_overlap=0, method='welch',
#                  eeg_bands = {'Delta': (0.5, 4), 'Theta': (4, 8), 'Alpha': (8, 12), 
#                  'Beta': (12, 30), 'Sigma':(12, 14), 'Gamma':(30, 49), 
#                  'Alpha1':(8, 10), 'Alpha2': (10, 12)})
#==============================================================================
    
    
    """
    Code below will pass throug all folders in list,
    calculate bands for each epoch and write them down
    into csv.
    """
    base_path = os.getcwd()
    path = '\\'.join((base_path, 'selected data\\'))
    directories = [x[1] for x in os.walk(path)]
    directories = directories[0]
    for f in directories:
        # Welch PSD calculation
        record = Signal(record_number=f, record_name='raw_filtered_1_40_normalized', 
                        epoch_sec=5, epoch_overlap=0.5, 
                        method='welch',signal_len=None,
                        sufix='.csv', separator='\\', fs=200, 
                        ch_num=6,
                        eeg_bands = {'Delta': (0.5, 4), 'Theta': (4, 8), 'Alpha': (8, 12), 
                                     'Beta': (12, 30), 'Sigma':(12, 14), 'Gamma':(30, 49), 
                                     'Alpha1':(8, 10), 'Alpha2': (10, 12)})
        
        # Haar wavelet PSD
# =============================================================================
#         record = Signal(record_number=f, record_name='raw_unfiltered', 
#                         epoch_sec=5, epoch_overlap=0.5, 
#                         method='Haar',signal_len=None,
#                         sufix='.csv', separator='\\', fs=200, 
#                         ch_num=6,
#                         eeg_bands = {'Delta': (0.5, 4), 'Theta': (4, 8), 'Alpha': (8, 13), 
#                                      'Beta': (13, 30), 'Gamma':(30, 50)})
# =============================================================================
        
        # multitapering PSD calculation
# =============================================================================
#         record = Signal(record_number=f, record_name='raw_filtered_1_40_normalized', 
#                         epoch_sec=5, epoch_overlap=0.5, base_path=path,
#                         method='fft',signal_len=None,
#                         sufix='.csv', separator='\\', fs=200, 
#                         ch_num=6,
#                         eeg_bands = {'Delta': (0.5, 4), 'Theta': (4, 8), 'Alpha': (8, 12), 
#                                      'Beta': (12, 30), 'Sigma':(12, 14), 'Gamma':(30, 49), 
#                                      'Alpha1':(8, 10), 'Alpha2': (10, 12)})
# =============================================================================
        print(f)
        
        for i, ch in enumerate(record.band):
            ppp = str(record.base_path + record.record_number + record.separator + 'channel' +str(i) + '_' + record.method + '.csv')
            np.savetxt(ppp, ch, delimiter=',')
            
    
    