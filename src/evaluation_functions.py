# -*- coding: utf-8 -*-
"""
Created on Thu Jul 16 15:24:27 2020

@author: Igor
"""


from scipy.stats import wilcoxon
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from xgboost import XGBClassifier
from sklearn.metrics import accuracy_score, precision_score, recall_score
import time
import sys

from generate_features import limit_max_change, limit_change_std, get_all_features




def get_p_value(a,b):
    return wilcoxon(a, b)[1]


def train_and_predict(X_train, y_train, X_test, y_test):
    start = time.time()
    for i in range(100):
        xgb = XGBClassifier()
        xgb.fit(X_train, y_train)
        y = xgb.predict(X_test)
        acc = accuracy_score(y_test, y)
        prec = precision_score(y_test, y)
        rec = recall_score(y_test, y)
    trajanje = (time.time() - start) / 100
    
    return acc, trajanje, prec, rec


def split_in_half_p_value(x):
    """
    Split x into two equal part and calculate wilcoxon p value
    """
    size = len(x)
    if size % 2 == 1:
        gg = int(np.floor(size/2))
        dg = int(np.ceil(size/2))
    else:
        gg = int(size/2)
        dg = gg
    
    #split
    first_half = x[:gg]
    second_half = x[dg:]
    
    # p value
    return get_p_value(first_half, second_half)


def which_half_is_larger(x):
    """
    Split x into two equal part and and check which mean is larger
    """
    size = len(x)
    if size % 2 == 1:
        gg = np.floor(size/2)
        dg = np.ceil(size/2)
    else:
        gg = int(size/2)
        dg = gg
    
    #split
    first_half = x[:gg]
    second_half = x[dg:]
    
    if np.mean(first_half) > np.mean(second_half):
        return 0
    return 1



def plot_all_16_subjects(subjects, max_change=None):
    """
    gets list of 16 subjects and plot each on one subplot
    """
    fig, axs = plt.subplots(4, 4, figsize=(15,15))
    for s, subject in enumerate(subjects):
        if max_change == None:
            sub = subject
        elif max_change == 'std':
            sub = limit_change_std(subject)
        else:
            sub = limit_max_change(subject, max_change=max_change)
        axs[int(s/4), s%4].plot(sub)
        axs[int(s/4), s%4].grid()
        axs[int(s/4), s%4].axvspan(119, 238, color='y', alpha=0.5)
    
    
def plot_all_features_for_single_subjects(subject):
    fig, axs = plt.subplots(9,1, figsize=(15,30))
    titles = ['Index1', 'Index2', 'θ/α', 'β/α', '(θ+α)/β', 'θ/β', '(θ+α)/(α+β)', 'γ/δ', '(γ+β)/(δ+α)']
    for i, f in enumerate(get_all_features()):
        if len(f[subject]) == 6:
            channels = ['F3', 'F4', 'C3', 'C4', 'O1', 'O2']
            for j, ch in enumerate(f[subject]):
                axs[i].plot(ch, label=channels[j])
            axs[i].legend(loc='best')
        else:
            axs[i].plot(f[subject])
        axs[i].grid()
        axs[i].axvspan(119, 238, color='y', alpha=0.5)
        axs[i].set_ylabel(titles[i])
        axs[i].set_xlabel('epochs')
        
        
        
        
if __name__ == '__main__':
    plot_all_features_for_single_subjects(9)
    