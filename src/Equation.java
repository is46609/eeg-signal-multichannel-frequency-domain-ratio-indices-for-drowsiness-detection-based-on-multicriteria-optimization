import java.util.ArrayList;
import java.util.List;

public class Equation {
	
	private int[] brojnik;
	private double[] brojnikCoeffs;
	private int[] nazivnik;
	private double[] nazivnikCoeffs;
	
	public Equation() {
		brojnik = new int[SDProblem.MAX_SIG_NUM*SDProblem.MAX_CH_NUM]; //[delta0]..[delta5][theta0]..[theta5][alpha0]..[beta0]..[sigma0]..[gamma0]..[alpha1]..[alpha2]
																	//if set to 0 skip, else if set to n take n vals: t_0 to t_n-1
		nazivnik = new int[SDProblem.MAX_SIG_NUM*SDProblem.MAX_CH_NUM];
		brojnikCoeffs = new double[SDProblem.MAX_SIG_NUM*SDProblem.MAX_CH_NUM];
		nazivnikCoeffs = new double[SDProblem.MAX_SIG_NUM*SDProblem.MAX_CH_NUM];
	}

	public int[] getBrojnik() {
		return brojnik;
	}
	
	public void setBrojnik(int [] brojnik) {
		this.brojnik = brojnik;
	}
	
	/*
	 * returns number of elements that are not set to 0
	 */
	public int getNumBrojnikElements() {
		int n=0;
		for (int i=0; i<SDProblem.MAX_SIG_NUM*SDProblem.MAX_CH_NUM; i++) {
			if(brojnik[i]>0) n++;
		}
		return n;		
	}

	public double[] getBrojnikCoeffs() {
		return brojnikCoeffs;
	}

	public int[] getNazivnik() {
		return nazivnik;
	}
	
	public void setNazivnik(int [] nazivnik) {
		this.nazivnik = nazivnik;
	}
	
	
	/*
	 * returns number of elements that are not set to 0
	 */
	public int getNumNazivnikElements() {
		int n=0;
		for (int i=0; i<SDProblem.MAX_SIG_NUM*SDProblem.MAX_CH_NUM; i++) {
			if(nazivnik[i]>0) n++;
		}
		return n;		
	}

	public double[] getNazivnikCoeffs() {
		return nazivnikCoeffs;
	}
	
	
	/*
	 * calculates the value of function for each value in trace
	 */
	public List<Double> calculateTrace(Trace trace) {
		
		List<Double> result = new ArrayList<Double>();
		
		for(int n=0; n<trace.getLength(); n++) {
			double res = 0.0;
			double resBrojnik = 0.0;
			double resNazivnik = 0.0;
			for(int i=0; i<SDProblem.MAX_SIG_NUM; i++) { //i -> signal type (delta, gamma...
				for (int j=0; j<SDProblem.MAX_CH_NUM; j++) { // j-> channel num: 0..5
					
					double slidingWindowRes = 0.0;
					int k=0;
					
					//brojnik
					if(brojnik[i*SDProblem.MAX_CH_NUM + j]>0) {						
						for(k=0; k<brojnik[i*SDProblem.MAX_CH_NUM + j]; k++) {
							if ((n-k)>=0) //do only if enough elements in past, else shorten window if no more vals in past
								slidingWindowRes+=trace.getSignals().get(i).getChannels().get(j).getVals().get(n-k); //get sum of all vals in sliding window
						}
						if((n-k)>=0)
							slidingWindowRes=slidingWindowRes/k; //average value of sliding window
						else
							slidingWindowRes=slidingWindowRes/(n+1);
						
						resBrojnik+=brojnikCoeffs[i*SDProblem.MAX_CH_NUM+j]*slidingWindowRes;
						
					}
					
					
					
					//nazivnik
					if(nazivnik[i*SDProblem.MAX_CH_NUM + j]>0) {
						slidingWindowRes = 0.0;
						for(k=0; k<nazivnik[i*SDProblem.MAX_CH_NUM + j]; k++) {
							if ((n-k)>=0)
								slidingWindowRes+=trace.getSignals().get(i).getChannels().get(j).getVals().get(n-k); //get sum of all vals in sliding window
						}
						if((n-k)>=0)
							slidingWindowRes=slidingWindowRes/k; //average value of sliding window
						else
							slidingWindowRes=slidingWindowRes/(n+1);
						
						resNazivnik+=nazivnikCoeffs[i*SDProblem.MAX_CH_NUM+j]*slidingWindowRes;
					}
					
					
				}
				
			}
			
			res=resBrojnik/resNazivnik;
			result.add(res);
		}
		
		return result;
	}
	
	
	/*
	 * calculates the value of function for each value in trace
	 */
	public List<Double> calculateTrace2(Trace trace) {
		
		List<Double> result = new ArrayList<Double>();
		
		for(int n=0; n<trace.getLength(); n++) {
			double res = 0.0;
			double resBrojnik = 0.0;
			double resNazivnik = 0.0;
			for(int i=0; i<SDProblem.MAX_SIG_NUM; i++) { //i -> signal type (delta, gamma...
				for (int j=0; j<SDProblem.MAX_CH_NUM; j++) { // j-> channel num: 0..5
					
					double slidingWindowRes = 0.0;
					int k=0;
					
					//brojnik
					if(brojnik[i*SDProblem.MAX_CH_NUM + j]>0) {						
						for(k=0; k<brojnik[i*SDProblem.MAX_CH_NUM + j]; k++) {
							if ((n-k)>=0) //do only if enough elements in past, else shorten window if no more vals in past
								slidingWindowRes+=trace.getSignals().get(i).getChannels().get(j).getVals().get(n-k); //get sum of all vals in sliding window
						}
						if((n-k)>=0)
							slidingWindowRes=slidingWindowRes/k; //average value of sliding window
						else
							slidingWindowRes=slidingWindowRes/(n+1);
						
						resBrojnik+=brojnikCoeffs[i*SDProblem.MAX_CH_NUM+j]*slidingWindowRes;
						
					}
					
					
					
					//nazivnik
					if(nazivnik[i*SDProblem.MAX_CH_NUM + j]>0) {
						slidingWindowRes = 0.0;
						for(k=0; k<nazivnik[i*SDProblem.MAX_CH_NUM + j]; k++) {
							if ((n-k)>=0)
								slidingWindowRes+=trace.getSignals().get(i).getChannels().get(j).getVals().get(n-k); //get sum of all vals in sliding window
						}
						if((n-k)>=0)
							slidingWindowRes=slidingWindowRes/k; //average value of sliding window
						else
							slidingWindowRes=slidingWindowRes/(n+1);
						
						resNazivnik+=nazivnikCoeffs[i*SDProblem.MAX_CH_NUM+j]*slidingWindowRes;
					}
					
					
				}
				
			}
			
			res=resBrojnik/resNazivnik;
			
			if(result.size()>0 && res>1.1*result.get(result.size()-1))
				res = 1.1*result.get(result.size()-1);
			else if(result.size()>0 && res< 0.9*result.get(result.size()-1))
				res = 0.9*result.get(result.size()-1);
			
			result.add(res);
		}
		
		return result;
	}

	

}
