import java.util.ArrayList;
import java.util.List;

public class Channel {
	
	private int id;
	private List<Double> vals;
	
	public Channel(int id) {
		
		if(id<0 || id>5)
			throw new IllegalArgumentException("Channel id must be between 0 and 5!");
		else {
			this.id=id;
			this.vals=new ArrayList<Double>();
		}
	}
	
	public int getID() {
		return id;
	}
	
	public List<Double> getVals(){
		return vals;
	}

}
