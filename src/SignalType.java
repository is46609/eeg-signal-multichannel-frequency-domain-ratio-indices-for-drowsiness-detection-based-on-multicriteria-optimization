
public enum SignalType {
	DELTA,
	THETA,
	ALPHA,
	BETA,
	SIGMA,
	GAMMA,
	ALPHA1,
	ALPHA2

}
