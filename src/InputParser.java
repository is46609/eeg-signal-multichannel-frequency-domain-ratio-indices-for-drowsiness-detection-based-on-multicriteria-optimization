import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class InputParser {
	
	public static int[] parseEquationVars(String vars) {
		 int[] var = new int [SDProblem.MAX_SIG_NUM*SDProblem.MAX_CH_NUM];
		 
		 String [] signalData = vars.split("\\|");
		 for (int i=0; i<signalData.length; i++) {
			 String [] sigDataParts = signalData[i].split(":");
			 
			 String [] channelData = sigDataParts[1].split(",");
			 for(int j=0; j<channelData.length; j++) {
				 String [] channelDataParts = channelData[j].split("-");
				 var[i*SDProblem.MAX_CH_NUM+ Integer.parseInt(channelDataParts[0])] =Integer.parseInt(channelDataParts[1]) + 1; //+1 because if set to 0 means is not used
			 }
		 }
		 
		 
		 return var;
	}
	
	public static List<Trace> parseTraces(String srcFolder) {
		
		File root = new File(srcFolder); // root directory
		File[] traceFolders= root.listFiles();
		
		List<Trace> traces = new ArrayList<Trace>();
		
		for (int i=0; i<traceFolders.length; i++) {
			if(traceFolders[i].isDirectory()) {
				Trace tr = new Trace(traceFolders[i].getName());	
				
				File [] files = traceFolders[i].listFiles();
				for (int j=0; j<files.length; j++) {
					String filename= files[j].getName();
					if(filename.contains("channel")) {
						int chNum= Integer.parseInt(String.valueOf(filename.charAt(filename.indexOf('.')-1)));
						
						try {
							List<String> lines = Files.readAllLines(Paths.get(files[j].getPath()));
							for(String l:lines) {
								String [] data = l.split(",");
								for(int n=0; n<SDProblem.MAX_SIG_NUM; n++) {
									tr.getSignals().get(n).getChannels().get(chNum).getVals().add(Double.parseDouble(data[n]));
								}
								
							}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				
				traces.add(tr);
			}
		}
		
		return traces;
	}

}
