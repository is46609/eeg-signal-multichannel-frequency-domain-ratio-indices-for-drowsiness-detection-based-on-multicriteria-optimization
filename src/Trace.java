import java.util.ArrayList;
import java.util.List;

public class Trace {
	
	private String id;
	private List<Signal> signals;
	
	public Trace(String id) {
		this.id=id;
		this.signals=new ArrayList<Signal>();
		signals.add(new Signal(SignalType.DELTA));
		signals.add(new Signal(SignalType.THETA));
		signals.add(new Signal(SignalType.ALPHA));
		signals.add(new Signal(SignalType.BETA));
		signals.add(new Signal(SignalType.SIGMA));
		signals.add(new Signal(SignalType.GAMMA));
		signals.add(new Signal(SignalType.ALPHA1));
		signals.add(new Signal(SignalType.ALPHA2));
		
	}
	
	public String getId() {
		return id;
	}
	
	public List<Signal> getSignals(){
		return signals;
	}
	
	public int getLength() {
		return signals.get(0).getChannels().get(0).getVals().size();
	}

}
