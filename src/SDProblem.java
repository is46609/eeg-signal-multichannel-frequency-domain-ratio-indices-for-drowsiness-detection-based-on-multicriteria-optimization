import java.util.List;

import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.EncodingUtils;
import org.moeaframework.problem.AbstractProblem;



public class SDProblem extends AbstractProblem{
	
	public static final int MAX_SIG_NUM = 8;
	public static final int MAX_CH_NUM = 6;
	public static final int NUM_OBJ = 2;
	public static double COEFF_UP_BOUND; //configurable in params file
	
	List<Trace> traces ; 
	private Equation eq;

	public SDProblem(List<Trace> traces, Equation eq, double coeffUpBound) {
		super(eq.getNumBrojnikElements()+eq.getNumNazivnikElements(), NUM_OBJ); 
		
		this.traces = traces;
		this.eq = eq;
		COEFF_UP_BOUND = coeffUpBound;
	}
	
	public List<Trace> getTraces (){
		return this.traces;
	}
	public Equation getEquation() {
		return eq;
	}
	
	@Override
	public Solution newSolution() {
		Solution solution = new Solution(numberOfVariables, numberOfObjectives);
		
		for (int v=0; v<numberOfVariables; v++){
			solution.setVariable(v, EncodingUtils.newReal(0.0, COEFF_UP_BOUND));
		}
		
		return solution;
	}
	

	@Override
	public void evaluate(Solution solution) {
		
		/*
		 * O1: sto veca razlika izmedu dva dijela,
		 * 			podjela moguca na bilo koja dva dijela, ali u svakoj polovici mora biti najmanje 30 tocaka
		 * 			izmedu dvije polovice je "prozor" sirine 30 (5%) unutar kojeg se ne ocitavaju vrijednosti jer se ocekuje skok
		 * O2: što manje jittera, jitter je raylika iyme]u dva susjedna o;itanja signala
		 */
		
		double [] halfDiff = new double [traces.size()];
		double [] jitter = new double [traces.size()];
		
		//set coeffs 
		int n=0;
		for(int k=0; k< eq.getBrojnik().length;k++) {
			if(eq.getBrojnik()[k]==0)
				eq.getBrojnikCoeffs()[k]=0;
			else {
				eq.getBrojnikCoeffs()[k]=EncodingUtils.getReal(solution.getVariable(n));
				n++;
			}
		}
		
		for(int k=0; k< eq.getNazivnik().length;k++) {
			if(eq.getNazivnik()[k]==0)
				eq.getNazivnikCoeffs()[k]=0;
			else {
				eq.getNazivnikCoeffs()[k]=EncodingUtils.getReal(solution.getVariable(n));
				n++;
			}
		}
				
		
		
		for(int i=0; i<traces.size(); i++) {						
			
			List<Double> calculation = eq.calculateTrace2(traces.get(i));
			
			//find window
			int startIndex=0;
			double diff = findDiffWindow(calculation, startIndex);									
			halfDiff[i]=diff;
								 
									
			//jitter
			double jittersNum = getJittersNum(calculation, startIndex, 30); //peak point = bigger by more than 25% than one of its two predecessors and in first half + 10%
			jitter[i]=jittersNum;
									
		}
		
		
		//calc objectives
		double avgWindowDiff = 0.0;
		double avgJitter = 0.0;
		for (int i=0; i<traces.size(); i++) {
			avgWindowDiff+=halfDiff[i];
			avgJitter+=jitter[i];
		}
		
		avgWindowDiff=avgWindowDiff/traces.size();
		avgJitter=avgJitter/traces.size();
		
		
		solution.setObjective(0, avgWindowDiff );
		solution.setObjective(1, avgJitter);		
		
	}


	
	
	
	
	
	public double[] getTrend(Trace trace) {
		
		 
		List<Double> calculation = eq.calculateTrace(trace);
		double [] trendPoints = new double [calculation.size()];
		
		for (int i=0; i<calculation.size(); i++) {
			if(i<2)
				trendPoints[i]=0;
				
			else
				trendPoints[i]=(calculation.get(i)- calculation.get(i-2))/2;
		}
		
		return trendPoints;
	}
	
	
	
	private double findDiffWindow(List <Double> calculation, int startIndex) {
		

		double [] diffs= new double[calculation.size()-90] ;
		
		for(int j=30; j<(calculation.size()-60); j++) {
			
			diffs[j-30]=0.0;
							
			//diff of avg val from all left from the window and avg val all right from the window
			double avgLeft = 0.0;
			double avgRight = 0.0;
			
			for(int k=0; k<j; k++) {
				avgLeft+=calculation.get(k);
			}
			avgLeft = avgLeft/j;
			
			
			for(int k=j+30; k<calculation.size(); k++) {
				avgRight+=calculation.get(k);
			}
			avgRight = avgRight/(calculation.size()-j-30);
			
			diffs[j-30] = Math.abs(avgRight-avgLeft);
			
		}
		
		//find biggest diff from diffs[]
		double maxDiff = 0.0;
     			
		for(int l=0; l<diffs.length; l++) {
			if(diffs[l]>maxDiff) {
				maxDiff = diffs[l];					
				startIndex = l+30;
			}
		}		
		
					
			
		//move "zero" to evaluate just diffs
		double lowestVal = getLowestValue(calculation);
		double movedZero = lowestVal- 0.01*lowestVal; //to avoid division with zero later
		
		//diff of avg val from all left from the window and avg val all right from the window
		double avgLeft = 0.0;
		double avgRight = 0.0;
		
		for(int k=0; k<startIndex; k++) {
			avgLeft+=calculation.get(k);
		}
		avgLeft = avgLeft/startIndex;
		
		
		for(int k=startIndex+30; k<calculation.size(); k++) {
			avgRight+=calculation.get(k);
		}
		avgRight = avgRight/(calculation.size()-startIndex-30);
		
		
		double absDiffPercent = Math.abs((avgRight-movedZero)-(avgLeft-movedZero))/Math.min((avgLeft-movedZero), (avgRight-movedZero));
		
		double halfDiff;
		if(absDiffPercent>=5.0) halfDiff = 1/absDiffPercent;
		else if (absDiffPercent>1.0) halfDiff = 10/absDiffPercent;
		else if (absDiffPercent>0.5) halfDiff = 100/absDiffPercent;
		else halfDiff = 1000;
		
		return halfDiff;
	}
	
	private double getJittersNum(List <Double> calculation, int windowIndex, int windowSize){
		double jittersNum = 0;
		

		//move "zero"
		double lowestVal = getLowestValue(calculation);
		double movedZero = lowestVal- 0.01*lowestVal; //to avoid division with zero later
		
		//left side
		for(int i=1; i<windowIndex; i++ ) {
			
			if(Math.abs((calculation.get(i)-movedZero)-(calculation.get(i-1)-movedZero))/Math.abs(calculation.get(i-1)-movedZero)>=5.0)
				jittersNum+=1000;
			else if(Math.abs((calculation.get(i)-movedZero)-(calculation.get(i-1)-movedZero))/Math.abs(calculation.get(i-1)-movedZero)>=1.0)
				jittersNum+=100;
			else if(Math.abs((calculation.get(i)-movedZero)-(calculation.get(i-1)-movedZero))/Math.abs(calculation.get(i-1)-movedZero)>=0.5)
				jittersNum+=10;
			else if(Math.abs((calculation.get(i)-movedZero)-(calculation.get(i-1)-movedZero))/Math.abs(calculation.get(i-1)-movedZero)>=0.25)
				jittersNum+=1;
			
		}
		
		//right side
		
		for(int i=windowIndex+windowSize; i<calculation.size(); i++ ) {
			if(Math.abs((calculation.get(i)-movedZero)-(calculation.get(i-1)-movedZero))/Math.abs(calculation.get(i-1)-movedZero)>=5.0)
				jittersNum+=1000;
			else if(Math.abs((calculation.get(i)-movedZero)-(calculation.get(i-1)-movedZero))/Math.abs(calculation.get(i-1)-movedZero)>=1.0)
				jittersNum+=100;
			else if(Math.abs((calculation.get(i)-movedZero)-(calculation.get(i-1)-movedZero))/Math.abs(calculation.get(i-1)-movedZero)>=0.5)
				jittersNum+=10;
			else if(Math.abs((calculation.get(i)-movedZero)-(calculation.get(i-1)-movedZero))/Math.abs(calculation.get(i-1)-movedZero)>=0.25)
				jittersNum+=1;
			
		}
		
		return jittersNum;
		
	}
	
	private double getLowestValue(List<Double> valueList) {
		double min= valueList.get(0);
		
		for(int i=1;i<valueList.size(); i++)
			if(valueList.get(i)<min) min=valueList.get(i);
		
		
		return min;
	}
	
	
}
