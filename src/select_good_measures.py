# -*- coding: utf-8 -*-
"""
Created on Sat Feb 22 14:44:30 2020

Iz training data selektira one zapise koji imaju min_sec_before sekundi 
stanja W (wake, budno) i odmah nakon toga min_sec_after sekundi 
stanja sna N1.

Sprema raw bez filtriranja i raw s filtriranjem.


@author: Giggs
"""
import wfdb
import numpy as np
import os
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter
from scipy.signal import freqz


def remove_dc_component(signal):
    mean = np.mean(signal)
    return signal-mean

### filters functions
def butter_bandpass(lowcut, highcut, fs, order=6):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=6):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

def plot_transfer_functions_for_orders(orders, lowcut=1, highcut=40):
    # Plot the frequency response for a few different orders.
    plt.figure(1)
    plt.clf()
    for order in orders:
        b, a = butter_bandpass(lowcut, highcut, fs, order=order)
        w, h = freqz(b, a, worN=2000)#np.logspace(-4, 3, 2000))
        plt.semilogx((fs * 0.5 / np.pi) * w, abs(h), label="order = %d" % order)  
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Gain')
    plt.grid(True)
    plt.legend(loc='best')
## Example
#plot_transfer_functions_for_orders([3,4,5,6], lowcut=0.5, highcut=40)

#################################################

# folder name
save_directory_name = 'selected data'

# conditions for selection
min_sec_before = 300
min_sec_after = 300

# Filtering parameters
lowcut = 1
highcut=40
order=6
fs = 200

##################################################

path = os.getcwd()
training_path = '\\'.join((path,'training data'))
directories = [x[1] for x in os.walk(training_path)]
directories = directories[0]
save_path_base = '\\'.join((path, save_directory_name))
# stvori dir ako ne postoji
if not os.path.isdir(save_path_base):
    os.mkdir(save_path_base)

for directory in directories:
    # ucitaj podatke
    full_path = '\\'.join((training_path, directory, directory))
    record = wfdb.rdrecord(full_path)
    ann = wfdb.rdann(full_path, extension='arousal')
    
    #==============================================================================
    # ann.__dict__['aux_note'] # oznake N1, W, respiratory shit...
    # ann.__dict__['sample'] # vremenska oznaka gdje je ovo iznad u signalu
    #==============================================================================
    
    started_flag = 0
    start = 0
    switch = 0
    endd = 0
    for i, comment in enumerate(ann.__dict__['aux_note']):
        if started_flag == 2 and (comment == 'N2' or comment == 'W'):
            endd = ann.__dict__['sample'][i]
            break
        elif started_flag == 1 and comment == 'N1':
            switch = ann.__dict__['sample'][i]
            started_flag = 2
        elif started_flag == 0 and comment == 'W':
            start = ann.__dict__['sample'][i]
            started_flag = 1
    
    # ako zadovoljava, odrezi i spremi
    if ((switch - start)/fs) >= min_sec_before and ((endd - switch)/fs) >= min_sec_after:
        # stvori dir ako ne postoji
        tmp_dir = '\\'.join((save_path_base, directory))
        if not os.path.isdir(tmp_dir):
            os.mkdir(tmp_dir)
        save_path = '\\'.join((tmp_dir, ''))
        
        min_diff_before = min_sec_before * fs
        min_diff_after = min_sec_after * fs
        cutted_signal = record.__dict__['p_signal'][switch-min_diff_before:switch+min_diff_after].T[0:6]
        np.savetxt(''.join((save_path, 'raw_unfiltered.csv')), cutted_signal, delimiter=",", fmt='%1.5f')
        filtered_signal = butter_bandpass_filter(cutted_signal, lowcut, highcut, fs, order=order)
        namee = ''.join(('raw_filtered_', str(lowcut), '_', str(highcut), '.csv'))
        np.savetxt(''.join((save_path, namee)), filtered_signal, delimiter=",", fmt='%1.5f')
        normalized_filtered_signal = remove_dc_component(filtered_signal)
        namee = ''.join(('raw_filtered_', str(lowcut), '_', str(highcut), '_normalized.csv'))
        np.savetxt(''.join((save_path, namee)), normalized_filtered_signal, delimiter=",", fmt='%1.5f')

