# -*- coding: utf-8 -*-
"""
Created on Thu Jul 16 17:48:47 2020

@author: Igor
"""

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from xgboost import XGBClassifier
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split

from evaluation_functions import train_and_predict, split_in_half_p_value, which_half_is_larger, plot_all_16_subjects
from generate_features import get_all_features

ga_folder = 'v4_2_3\\run-daaa-0\\10Mgens'
base_path = os.getcwd()

header = ['subject', 'Index_1', 'Index_2', 't/a', 'b/a', '(t+a)/b', 't/b', '(t+a)/(a+b)', 'g/d', '(g+b)/(d+a)']
subjects = ['tr03-0092','tr03-0256','tr03-0876','tr03-1389','tr04-0649','tr04-0726','tr05-1434','tr05-1675','tr07-0168','tr07-0458','tr07-0861','tr08-0021','tr08-0111','tr09-0175','tr10-0872','tr13-0204']


def counting_evaluation(y, half_len=119):
    counter = 0
    for start in range(16):
        zero = sum(y[start*2*half_len:start*2*half_len + half_len])
        one = sum(y[start*2*half_len + half_len:start*2*half_len + 2*half_len])
        if one > (half_len-39) and zero < (half_len-79):
            counter += 1
    return counter


          
def generate_excel(data, output_file):
    """
    Get list with values for table and subjects is number of rows.
    """
    if len(data)%len(subjects) != 0:
        raise ValueError('len(data) mora biti visekratnik od subject')
    if '.xlsx' not in output_file:
        raise ValueError('.xlsx extension must be in output file')
    
    df = pd.DataFrame(columns=header)
    
    for i, subject in enumerate(subjects):
        tmp = [subject]
        for j in range(len(header)-1):
            tmp.append(data[i*(len(header)-1)+j])
        
        df = df.append(pd.Series(data=np.array(tmp), index=header), ignore_index=True)
    
    df.to_excel(output_file)


def find_best_unit_of_ga(alpha=0.0001, max_change=None):
    """
    Finds best unit of genetic algoritm
    Print several evaluations of each file
    print: (file name, no of step like p_value users, no of anti step like p_value users, sum of previous two
            xgb accuracy for every single epoch, no of "correctly predicted" users with step like function,
            number of coeficients equal to zero)
    """
    path = '\\'.join((base_path, ga_folder))
    scaler = MinMaxScaler()
    
    user = []
    all_users_flag = True
    all_users_y = []
    
    for f in os.listdir(path):
        step_counter = 0
        anti_step_counter = 0
        file_path = '\\'.join((path, f))
        with open(file_path) as fi:
            lines = fi.readlines()
            flag = False
            hashtag = True
            hashtag_counter = 0
            zeroes = 0
            for line in lines:
                # broji koeficijente == 0 u fileu
                if hashtag:
                    if line[0] == '#':
                        hashtag_counter += 1
                        if hashtag_counter >= 4:
                            hashtag = False
                        continue
                    elif line[0] == '0':
                        zeroes += 1
                
                # Izvlaci vrijednosti iz filea
                if flag: 
                    if '?' not in line:
                        user.append(float(line.split(',')[0]))
                    else:
                        # limit max change between two points
                        if max_change != None:
                            user = limit_max_change(user, max_change=max_change)
                        # pvalue evaluation
                        half_len= int(len(user)/2)
                        pval = split_in_half_p_value(user)
                        if pval < alpha:
                            if which_half_is_larger(user):
                                step_counter += 1
                            else:
                                anti_step_counter += 1
                                
                        # store user for xgb
                        user = np.array(user).reshape(-1, 1)
                        if all_users_flag:
                            all_users = user
                            all_users_flag = False
                        else:
                            all_users = np.concatenate((all_users,scaler.fit_transform(user)), axis=0)
                            #%all_users = np.concatenate((all_users,user), axis=0)
                        all_users_y.extend([0]*half_len)
                        all_users_y.extend([1]*half_len)
                        
                        # reset variables
                        flag = False
                        user = []
                else:
                    if '?' in line:
                        flag = True
                    else:
                        continue
                    
        # xgboost
        xgb = XGBClassifier()
        X_train, X_test, y_train, y_test = train_test_split(all_users, all_users_y, test_size=0.33, random_state=42)
        xgb.fit(X_train, y_train)
        y_pred = xgb.predict(X_test)
        acc = accuracy_score(y_test, y_pred)
        y_pred = xgb.predict(all_users)
        ccc = counting_evaluation(y_pred)
        print((f, step_counter, anti_step_counter, step_counter+anti_step_counter, acc, ccc, zeroes))
        
        # reset
        all_users_flag = True
        all_users_y = []
        
        
def plot_all_users(file='res_33.txt', max_change=None):
    fig, axs = plt.subplots(4, 4, figsize=(15,15))
    
    user = []
    i=0
    j=0
    path = '\\'.join((base_path, ga_folder))
    file_path = '\\'.join((path, file))
    with open(file_path) as fi:
        lines = fi.readlines()
        flag = False
        for line in lines:
            if flag:
                if '?' not in line:
                    user.append(float(line.split(',')[0]))
                else:
                    # limit max change between two points
                    if max_change != None:
                        user = limit_max_change(user, max_change=max_change)
                    # plot
                    axs[i, j].plot(user)
                    axs[i, j].grid()
                    axs[i, j].axvspan(119, 238, color='y', alpha=0.5)
                    j+=1
                    if j == 4:
                        j=0
                        i+=1
                        if i==4:
                            break
                    
                    # reset variables
                    flag = False
                    user = []
            else:
                if '?' in line:
                    flag = True
                else:
                    continue
    pass
        
def find_best_electrode(channels):
    """
    Pronade koja je najbolja elektroda i vrati njene vrijednosti
    """
    mean = 100
    best = None
    for i, c in enumerate(channels):
        if c.mean() < mean:
            mean =  c.mean()
            best = i
    return channels[best].reshape(-1, 1)


def p_val_for_all():
    """
    p_value for each subject
    """
    for i, feat in enumerate(get_all_features()):
        tmp = []
        flag_find_best_channel = False
        for j, subject in enumerate(feat):
            if isinstance(subject, list) and len(subject)!=238:
                ch_tmp = []
                for channel in subject:
                    ch_tmp.append(split_in_half_p_value(channel))
                ch_tmp = np.array(ch_tmp).reshape(-1, 1)
                if j == 0:
                    all_ch_tmp = ch_tmp
                else:
                    all_ch_tmp = np.concatenate((all_ch_tmp, ch_tmp), axis=1)
                flag_find_best_channel = True
            else:
                tmp.append(split_in_half_p_value(subject))
        if flag_find_best_channel:
            tmp = find_best_electrode(all_ch_tmp)
        else:
            tmp = np.array(tmp).reshape(-1, 1)
        if i == 0:
            p_vals = tmp
        else:
            p_vals = np.concatenate((p_vals, tmp), axis=1)
    p_vals = p_vals.ravel()
    generate_excel(p_vals, 'all_p_vals.xlsx')
    
    
def xgb_for_all(acc_output='all_xgb_acc.xlsx', time_output='all_xgb_time.xlsx', 
                prec_output='all_xgb_precision.xlsx', rec_output='all_xgb_recall.xlsx'):
    """
    p_value for each subject
    """
    for i, feat in enumerate(get_all_features()):
        no_subjects = len(feat)
        y_test = [0]*119
        y_test.extend([1]*119)
        y_train = y_test*(no_subjects-1)
        tmp_acc = []
        tmp_prec = []
        tmp_rec = []
        tmp_time = []
        for j, _X_test in enumerate(feat):
            # y_train, y_test
            if isinstance(_X_test, list) and len(_X_test)!=238 and len(_X_test[0]) == 237:
                y_test = [0]*119
                y_test.extend([1]*118)
                y_train = y_test*(no_subjects-1)
            
            # X_test
            if isinstance(_X_test, list) and len(_X_test)!=238:
                for z, chh in enumerate(_X_test):
                    if isinstance(chh, list):
                        ch = np.array(chh)
                    else:
                        ch = chh
                    
                    if z==0:
                        tmp_ch = ch.reshape(-1,1)
                    else:
                        tmp_ch = np.concatenate((tmp_ch, ch.reshape(-1,1)),axis=1)
                X_test = tmp_ch
            else:
                X_test = np.array(_X_test).reshape(-1,1)
            
            # X_train6
            for k, subject in enumerate(feat):
                if k==j:
                    continue
                if isinstance(subject, list) and len(subject)!=238:
                    for z, chh in enumerate(subject):
                        if isinstance(chh, list):
                            ch = np.array(chh)
                        else:
                            ch = chh
                            
                        if z==0:
                            tmp_ch = ch.reshape(-1,1)
                        else:
                            tmp_ch = np.concatenate((tmp_ch, ch.reshape(-1,1)),axis=1)
                    if k==0 or (j==0 and k==1):
                        X_train = tmp_ch
                    else:
                        X_train = np.concatenate((X_train, tmp_ch))
                else:
                    if k==0 or (j==0 and k==1):
                        X_train = np.array(subject).reshape(-1,1)
                    else:
                        _subject = np.array(subject).reshape(-1,1)
                        X_train = np.concatenate((X_train, _subject))
            
            acc, timee, prec, rec = train_and_predict(X_train, y_train, X_test, y_test)
            tmp_acc.append(acc)
            tmp_prec.append(prec)
            tmp_rec.append(rec)
            tmp_time.append(timee)
        tmp_acc = np.array(tmp_acc).reshape(-1,1)
        tmp_prec = np.array(tmp_prec).reshape(-1,1)
        tmp_rec = np.array(tmp_rec).reshape(-1,1)
        tmp_time = np.array(tmp_time).reshape(-1,1)
        print(tmp_acc.mean())
        print(tmp_time.mean())
        print(tmp_prec.mean())
        print(tmp_rec.mean())
        if i == 0:
            accc = tmp_acc
            preccc = tmp_prec
            reccc = tmp_rec
            timeee = tmp_time
        else:
            accc = np.concatenate((accc, tmp_acc), axis=1)
            preccc = np.concatenate((preccc, tmp_prec), axis=1)
            reccc = np.concatenate((reccc, tmp_rec), axis=1)
            timeee = np.concatenate((timeee, tmp_time), axis=1)
    accc = accc.ravel()
    preccc = preccc.ravel()
    reccc = reccc.ravel()
    timeee = timeee.ravel()
    generate_excel(accc, acc_output)
    generate_excel(preccc, prec_output)
    generate_excel(reccc, rec_output)
    generate_excel(timeee, time_output)
    

if __name__ == '__main__':
    #find_best_unit_of_ga()
    #find_best_unit_of_ga(max_change=1.001)
    #plot_all_users(file='res_33.txt', max_change=None)
    #plot_all_users(file='res_7.txt', max_change=1.001)
    #plot_all_16_subjects(get_novel_index_1(max_change=1.1))
    #p_val_for_all()
    xgb_for_all()
    #xgb_for_all(acc_output='limit_change_xgb_acc.xlsx', time_output='limit_change_xgb_time.xlsx')